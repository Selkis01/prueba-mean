import { IUserModel } from './user.model';
import { ICreditModel } from './credit.model';

export interface ICreditRequestModel extends IUserModel, ICreditModel {

}

