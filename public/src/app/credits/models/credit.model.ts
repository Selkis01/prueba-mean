import { IUserModel } from './user.model';

export interface ICreditModel {
    status: boolean;
    pay: boolean;
    value: number;
    datePay: Date;
    user: IUserModel;
}

