import { ICreditModel } from './credit.model';

export interface IUserModel {
    name: string;
    email: string;
    document: string;
}
