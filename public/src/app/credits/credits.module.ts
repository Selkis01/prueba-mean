import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { CreditsRoutingModule } from './credits-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { MountComponent } from './components/mount/mount.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ListComponent,
    CreateComponent,
    MountComponent
  ],
  imports: [
    CommonModule,
    CreditsRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    ListComponent,
    CreateComponent,
    MountComponent
  ]

})
export class CreditsModule { }
