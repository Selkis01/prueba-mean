import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICreditModel } from './../models/credit.model';
import { ICreditRequestModel } from './../models/creditRequest.model';

@Injectable({
  providedIn: 'root'
})
export class CreditsService {

  constructor(private http: HttpClient) { }


  public getListCredits(): Observable<ICreditModel[]> {
    return this.http.get<ICreditModel[]>(
      'http://localhost:3000/api/credits');
  }

  public postCreateCredit(form: ICreditRequestModel): Observable<any> {
    console.log(form);
    return this.http.post<any>(
      'http://localhost:3000/api/credits', form);
  }

}
