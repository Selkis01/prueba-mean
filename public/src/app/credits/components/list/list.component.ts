import { Component, OnInit } from '@angular/core';
import { CreditsService } from './../../services/credits.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public credits;

  constructor(private service: CreditsService) { }

  /**
   * To init
   */
  public ngOnInit(): void {
    this.getCredits();

  }

  /**
   * To list credits
   */
  public getCredits(): void {
    this.service.getListCredits().subscribe(res => {
      console.log(res);
      this.credits = res;
    });
  }

}
