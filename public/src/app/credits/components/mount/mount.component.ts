import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mount',
  templateUrl: './mount.component.html',
  styleUrls: ['./mount.component.scss']
})
export class MountComponent {

  constructor() { }

  @Output() private changeValue = new EventEmitter<any>();
  /**
   * To emit value
   * @param newValue any
   */
  public onChange(newValue): void {
    this.changeValue.emit(newValue);
  }

}
