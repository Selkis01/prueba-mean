import { Component } from '@angular/core';
import { environment } from './../../../../environments/environment';
import { ICreditRequestModel } from './../../models/creditRequest.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CreditsService } from './../../services/credits.service';


@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  public montoDisponible = environment.montoBase;
  public form: FormGroup;
  public credit: ICreditRequestModel;
  public model: any;
  public value: any;

  constructor(
    private formBuilder: FormBuilder,
    private service: CreditsService
  ) {
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      document: ['', Validators.required],
      datePay: ['', '']
    });
  }

  /**
   * To change value
   * @param $event 
   */
  public change($event): void {
    console.log($event);
    this.value = $event;
  }

  /**
   * To create
   */
  public create(): void {
    if (this.form.invalid) {
      return;
    }

    if (this.value > this.montoDisponible) {
      alert("Ya no hay mas cupo disponible");
      return;
    }

    this.credit = this.form.value;
    this.credit.value = this.value;
    this.credit.datePay = new Date(this.form.controls.datePay.value.day,
      this.form.controls.datePay.value.month,
      this.form.controls.datePay.value.year);

    this.service.postCreateCredit(this.credit).subscribe(res => {
      this.montoDisponible = this.montoDisponible - this.credit.value;
      alert(res.message);
    });
  }

}
