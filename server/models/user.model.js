const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    name: { type: String, require: true },
    email: { type: String, require: true },
    document: { type: String, require: true, unique: true },
    credits: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Credit' }
    ]
});

module.exports = mongoose.model('User', userSchema);