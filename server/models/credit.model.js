const mongoose = require('mongoose');
const { Schema } = mongoose;

const creditSchema = new Schema({
    value: { type: Number, require: true },
    datePay: { type: Date, require: false },
    status: { type: Boolean, require: true, default: false },
    pay: { type: Boolean, require: true, default: false },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
});

module.exports = mongoose.model('Credit', creditSchema);