const Credit = require('../models/credit.model');
const User = require('../models/user.model');
const { check, validationResult } = require('express-validator');


const creditController = {};

creditController.getCredits = async(req, res) => {
    const credits = await Credit.find().populate('user');
    return res.send(credits);
};

creditController.getUsers = async(req, res) => {
    const users = await User.find();
    if (users.length < 1) {
        res.json({
            code: 204,
            message: "No hay creditos"
        })
    } else {
        res.json({
            code: 200,
            message: "Operación Exitosa",
            users: users
        })
    }
};

creditController.getUsersCredits = async(req, res) => {
    const users = await User.findById(req.params.id).populate('credits');
    if (users.length < 1) {
        res.json({
            code: 204,
            message: "No hay creditos"
        })
    } else {
        res.json({
            code: 200,
            message: "Operación Exitosa",
            users: users.credits
        })
    }
};

creditController.addCredit = async(userId, credit) => {
    credit.user = userId;
    return Credit.create(credit).then(creditCr => {
        return User.findByIdAndUpdate(
            userId, { $push: { credits: creditCr } }, { new: true, useFindAndModify: false }
        );
    });
}

creditController.createUser = async(user) => {
    return User.create(user).then(userCr => {
        return userCr;
    });
};

creditController.createCredit = async(req, res) => {

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    let userById = await User.findOne({ document: req.body.document });
    const userParm = new User(req.body);
    const credit = new Credit(req.body);

    if (userById == null) {
        userById = await creditController.createUser(userParm);
    }
    const credits = userById.populate('credits');
    credit.status = credits.credits.length < 1 ? true : Math.random() >= 0.5;
    await creditController.addCredit(userById, credit);

    res.json({
        code: 201,
        message: "Operación Exitosa"
    });

};

creditController.getCredit = (req, res) => {
    const credit = Credit.findById(req.params.id);
    Credit.findById(req.params.id)
        .then((credit) => {
            res.json({
                code: 200,
                message: "Operación Exitosa",
                employee: employee
            });
        })
        .catch((err) => {
            console.log(err);
            res.json({
                code: 500,
                message: err.message
            });
        });
};

creditController.updateCredit = async(req, res) => {
    const credit = {
        name: req.body.name,
        email: req.body.position,
        document: req.body.office,
        value: req.body.salary,
        datePay: req.body.salary,
        status: req.body.salary,
        pay: req.body.salary
    }

    await Credit.findByIdAndUpdate(req.params.id, { $set: credit }, { new: true })
        .then((response) => {
            res.json({
                code: 200,
                message: "Operación Exitosa",
            });
        })
        .catch((err) => {
            console.log(err);
            res.json({
                code: 500,
                message: err.message
            });
        });
};

creditController.deleteCredit = async(req, res) => {
    Credit.findByIdAndRemove(req.params.id)
        .then((response) => {
            res.json({
                code: 200,
                message: "Operación Exitosa",
            });
        })
        .catch((err) => {
            console.log(err);
            res.json({
                code: 500,
                message: err.message
            });
        });
};

module.exports = creditController;