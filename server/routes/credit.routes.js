const express = require('express');
const { body, validationResult } = require('express-validator');

const router = express.Router();

const creditController = require('../controllers/credit.controller');

router.get('/', creditController.getCredits);
router.post('/', creditController.createCredit);

module.exports = router;